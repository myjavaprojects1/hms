package org.ass.hms.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	public static Date getDate(String date) {
	
	Date d;
	try {
		d = new SimpleDateFormat("dd/MM/yyyy").parse(date);
		return d;
	} catch (ParseException e) {
	    return null;
	}
	
}
}