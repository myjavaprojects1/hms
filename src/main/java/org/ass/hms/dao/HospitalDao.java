package org.ass.hms.dao;


import java.util.Date;
import java.util.List;

import org.ass.hms.dto.AppointmentDto;
import org.ass.hms.dto.UserAndDoctorDto;
import org.ass.hms.entity.AppointmentDetails;

public interface HospitalDao {

	public void create(UserAndDoctorDto uadDto);
	public List<AppointmentDetails> getAllAppointmentOfaDoctorByDate(String date);
	public void bookAppointment(AppointmentDto Dto);
	public void updateContactByEmail(String contact,String email);
	public void updateRemarksByPatientId(long pid,String remarks);
	public List<AppointmentDetails> getAllAppointmentByDoctorIdAndDate(long did,String appointmentDate);
}
