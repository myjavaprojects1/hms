package org.ass.hms.util;


import org.ass.hms.entity.AppointmentDetails;
import org.ass.hms.entity.Doctor;
import org.ass.hms.entity.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryUtil {
	private static SessionFactory sessionFactory;
	public SessionFactoryUtil() {}
	
	static{
		Configuration cfg=new Configuration();
		cfg.setProperties(ConnectionPropertiesUtil.getConnectionProperties());
		cfg.addAnnotatedClass(Doctor.class);
		cfg.addAnnotatedClass(User.class);
		cfg.addAnnotatedClass(AppointmentDetails.class);
		sessionFactory=cfg.buildSessionFactory();
	}
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

}
