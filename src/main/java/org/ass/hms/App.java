package org.ass.hms;

import java.util.List;

import org.ass.hms.dao.HospitalDaoImpl;
import org.ass.hms.dto.AppointmentDto;
import org.ass.hms.dto.UserAndDoctorDto;
import org.ass.hms.entity.AppointmentDetails;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
//    	UserAndDoctorDto uadDto = new UserAndDoctorDto();
//    	uadDto.setUname("ramanan");
//    	uadDto.setUemail("raman@gmail.com");
//    	uadDto.setUcontact("7876677810");
//    	uadDto.setDname("balaji");
//    	uadDto.setDemail("baldoct@gmail.com");
//    	uadDto.setDcontact("989807654");
//    	uadDto.setDavailableTime("08,03,08");
//    	uadDto.setDtype("cardiologist");

        HospitalDaoImpl h = new HospitalDaoImpl();
//        h.create(uadDto);
//        List<AppointmentDetails> list = h.getAllAppointmentOfaDoctorByDate("13/10/2023");
//        for(AppointmentDetails ad:list) {
//        	System.out.println(ad);
//        }
//        System.out.println("****************************************");
//        AppointmentDto appointmentDto = new AppointmentDto();
//        appointmentDto.setUserId(6);
//        appointmentDto.setDoctorId(1);
//        appointmentDto.setTime("10");
//        appointmentDto.setAppointmentDate("13/10/2023");
//        h.bookAppointment(appointmentDto);
//        h.updateContactByEmail("123456", "ram@gmail.com");
//        h.updateRemarksByPatientId(2, "neat explanation");
        
        List<AppointmentDetails> list1 = h.getAllAppointmentByDoctorIdAndDate(2, "11/10/2023");
        for(AppointmentDetails ad:list1) {
      	  System.out.println(ad);
        }
      
    }
}
