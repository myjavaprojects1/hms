package org.ass.hms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="appointment_details")
public class AppointmentDetails implements Serializable {
	
	@Id
	@GenericGenerator(name="reg_auto", strategy="increment")
	@GeneratedValue(generator="reg_auto")
	@Column(name="appointment_id")
	private long id;
	
	@Column(name="p_id")
	private long pid;
	
	@Column(name="d_id")
	private long did;
	
	@Column(name="appointment_date")
	private Date appointmentDate;
	
	@Column(name="remarks")
	private String remarks;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPid() {
		return pid;
	}

	public void setPid(long pid) {
		this.pid = pid;
	}

	public long getDid() {
		return did;
	}

	public void setDid(long did) {
		this.did = did;
	}

	public Date getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "AppointmentDetails [id=" + id + ", pid=" + pid + ", did=" + did + ", appointmentDate=" + appointmentDate
				+ ", remarks=" + remarks + "]";
	}
	
	
	

}
