package org.ass.hms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="doctor_info")
public class Doctor implements Serializable  {
	
	@Id
	@GenericGenerator(name="reg_auto", strategy="increment")
	@GeneratedValue(generator="reg_auto")
	@Column(name="doctor_id")
	private long id;
	
	@Column(name="doctor_name")
	private String name;
	
	@Column(name="doctor_email")
	private String email;
	
	@Column(name="doctor_contact")
	private String contact;
	
	@Column(name="doctor_type")
	private String type;
	
	@Column(name="available_time")
	private String availableTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAvailableTime() {
		return availableTime;
	}

	public void setAvailableTime(String availableTime) {
		this.availableTime = availableTime;
	}
	
	

}
