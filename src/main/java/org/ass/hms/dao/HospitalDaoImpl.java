package org.ass.hms.dao;

import java.util.Date;
import java.util.List;

import org.ass.hms.dto.AppointmentDto;
import org.ass.hms.dto.UserAndDoctorDto;
import org.ass.hms.entity.AppointmentDetails;
import org.ass.hms.entity.Doctor;
import org.ass.hms.entity.User;
import org.ass.hms.util.DateUtil;
import org.ass.hms.util.SessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class HospitalDaoImpl implements HospitalDao {

	@Override
	public void create(UserAndDoctorDto uadDto) {
		User user = new User();
		user.setName(uadDto.getUname());
		user.setEmail(uadDto.getUemail());
		user.setContact(uadDto.getUcontact());
		Doctor doctor=new Doctor();
		doctor.setName(uadDto.getDname());
		doctor.setEmail(uadDto.getDemail());
		doctor.setType(uadDto.getDtype());
		doctor.setContact(uadDto.getDcontact());
		doctor.setAvailableTime(uadDto.getDavailableTime());
		
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.merge(user);
		session.merge(doctor);
		transaction.commit();
	}
	
	@Override
	public List<AppointmentDetails> getAllAppointmentOfaDoctorByDate(String date) {
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		StringBuilder builder = new StringBuilder();
		builder.append("from AppointmentDetails where appointmentDate=:d");
		
		Query query = session.createQuery(builder.toString());
		query.setParameter("d", DateUtil.getDate(date));
		List list = query.getResultList();
		
		return list;
	}

	@Override
	public void bookAppointment(AppointmentDto dto) {
		AppointmentDetails ad = new AppointmentDetails();
		ad.setPid(dto.getUserId());
		ad.setDid(dto.getDoctorId());
		ad.setAppointmentDate(DateUtil.getDate(dto.getAppointmentDate()));
		
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Doctor doctor = session.get(Doctor.class, dto.getDoctorId());
		
			if(doctor.getAvailableTime().contains(dto.getTime())) {
				List<AppointmentDetails> list = getAllAppointmentByDoctorIdAndDate(dto.getDoctorId(),dto.getAppointmentDate());
				int length = list.size();
					if(length<4) session.merge(ad); 
					else System.out.println("Appointment get filled for this date and doctor");
			}
			else System.out.println("this time slot is not available for this doctor");
		
		transaction.commit();
		
	}

	@Override
	public void updateContactByEmail(String contact, String email) {
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.createQuery("update User set contact=:c where email=:e");
		query.setParameter("c", contact);
		query.setParameter("e", email);
		query.executeUpdate();
		transaction.commit();
		
	}

	@Override
	public void updateRemarksByPatientId(long pid, String remarks) {
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.createQuery("update AppointmentDetails set remarks=:r where pid=:p");
		query.setParameter("r",remarks );
		query.setParameter("p", pid);
		query.executeUpdate();
		transaction.commit();
		
	}

	@Override
	public List<AppointmentDetails> getAllAppointmentByDoctorIdAndDate(long did, String appointmentDate) {
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.createQuery("from AppointmentDetails where did=:d and appointmentDate=:ad");
		query.setParameter("d",did );
		query.setParameter("ad",DateUtil.getDate(appointmentDate));
		List list = query.getResultList();
		transaction.commit();
		return list;
	}
}
