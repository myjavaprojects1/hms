package org.ass.hms.util;

import java.util.Properties;

import org.hibernate.cfg.Environment;

public class ConnectionPropertiesUtil {
	public static Properties getConnectionProperties() {
	Properties properties=new Properties();
	properties.setProperty(Environment.URL, "jdbc:mysql://localhost:3306/hospital_db");
	properties.setProperty(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
	properties.setProperty(Environment.USER, "root");
	properties.setProperty(Environment.PASS, "root");
	properties.setProperty(Environment.SHOW_SQL,"true");
	properties.setProperty(Environment.DIALECT,"org.hibernate.dialect.MySQL8Dialect");
	return properties;
}
}

