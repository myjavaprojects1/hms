package org.ass.hms.dto;

public class UserAndDoctorDto {
	private String dname;
	
	private String demail;
	
	private String dcontact;
	
	private String dtype;
	
	private String davailableTime;
	
	private String uname;
	
	private String uemail;
	
	private String ucontact;

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getDemail() {
		return demail;
	}

	public void setDemail(String demail) {
		this.demail = demail;
	}

	public String getDcontact() {
		return dcontact;
	}

	public void setDcontact(String dcontact) {
		this.dcontact = dcontact;
	}

	public String getDtype() {
		return dtype;
	}

	public void setDtype(String dtype) {
		this.dtype = dtype;
	}

	public String getDavailableTime() {
		return davailableTime;
	}

	public void setDavailableTime(String davailableTime) {
		this.davailableTime = davailableTime;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUemail() {
		return uemail;
	}

	public void setUemail(String uemail) {
		this.uemail = uemail;
	}

	public String getUcontact() {
		return ucontact;
	}

	public void setUcontact(String ucontact) {
		this.ucontact = ucontact;
	}
	
	
}
